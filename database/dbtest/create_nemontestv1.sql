# Init DB
DROP DATABASE IF EXISTS nemontestv1;
CREATE DATABASE nemontestv1 CHARACTER SET utf8 COLLATE utf8_general_ci;
USE nemontestv1;

# Create tables
CREATE TABLE precio_regulado (
    `id` INT AUTO_INCREMENT,
    `tipo` VARCHAR(10) DEFAULT '',
    `tarifa` VARCHAR(10) DEFAULT '',
    `fecha_inicio` DATE DEFAULT NULL,
    `fecha_fin` DATE DEFAULT NULL,
    `p1` DOUBLE DEFAULT 0,
    `p2` DOUBLE DEFAULT 0,
    `p3` DOUBLE DEFAULT 0,
    `p4` DOUBLE DEFAULT 0,
    `p5` DOUBLE DEFAULT 0,
    `p6` DOUBLE DEFAULT 0,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `tipo` ON `precio_regulado` (`tipo`);
CREATE INDEX `tarifa` ON `precio_regulado` (`tarifa`);
CREATE INDEX `fecha_inicio` ON `precio_regulado` (`fecha_inicio`);
CREATE INDEX `fecha_fin` ON `precio_regulado` (`fecha_fin`);
CREATE INDEX `multiIndex1` ON `precio_regulado` (`tipo`, `tarifa`, `fecha_inicio`, `fecha_fin`);

CREATE TABLE cliente (
    `id` INT AUTO_INCREMENT,
    `nombre` VARCHAR(80) DEFAULT '',
    `identificador_fiscal` VARCHAR(30) DEFAULT '',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `nombre` ON `cliente` (`nombre`);
CREATE INDEX `identificador_fiscal` ON `cliente` (`identificador_fiscal`);

CREATE TABLE producto (
    `id` INT AUTO_INCREMENT,
    `nombre` VARCHAR(80) DEFAULT '',
    `precio_venta` DOUBLE DEFAULT 0,
    `precio_coste` DOUBLE DEFAULT 0,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `precio_coste` ON `producto` (`precio_coste`);
CREATE INDEX `precio_venta` ON `producto` (`precio_venta`);

CREATE TABLE factura (
    `id` INT AUTO_INCREMENT,
    `cliente_id` INT DEFAULT NULL,
    `base_imponible` DOUBLE DEFAULT 0,
    `iva` DOUBLE DEFAULT 0,
    `total` DOUBLE DEFAULT 0,
    `estado_pago` VARCHAR(20) DEFAULT 'pendiente',
    PRIMARY KEY (id),
    FOREIGN KEY (`cliente_id`) REFERENCES `cliente`(`id`) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `estado_pago` ON `factura` (`estado_pago`);

CREATE TABLE factura_linea (
    `id` INT AUTO_INCREMENT,
    `factura_id` INT DEFAULT NULL,
    `producto_id` INT DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (`factura_id`) REFERENCES `factura`(`id`) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (`producto_id`) REFERENCES `producto`(`id`) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Insert data
INSERT INTO precio_regulado
VALUES
(1, 'energia', '2.0TD', '2021-01-01', '2021-12-31', 0.133118, 0.041772, 0.006001, 0, 0, 0),
(2, 'energia', '2.0TD', '2022-01-01', '2400-12-31', 0.233118, 0.141772, 0.106001, 0, 0, 0),
(3, 'potencia', '2.0TD', '2021-01-01', '2021-12-31', 30.67266, 1.424359, 0, 0, 0, 0),
(4, 'potencia', '2.0TD', '2022-01-01', '2400-12-31', 45.67266, 5.141772, 0, 0, 0, 0),
(5, 'energia', '3.0TD', '2021-01-01', '2021-12-31', 0.077436, 0.05931, 0.032102, 0.017413, 0.007897, 0.005056),
(6, 'energia', '3.0TD', '2022-01-01', '2400-12-31', 0.177436, 0.15931, 0.132102, 0.117413, 0.107897, 0.105056),
(7, 'potencia', '3.0TD', '2021-01-01', '2021-12-31', 19.596985, 13.781919, 7.005384, 6.106183, 6.106183, 2.636993),
(8, 'potencia', '3.0TD', '2022-01-01', '2400-12-31', 30.596985, 25.781919, 14.005384, 12.106183, 12.106183, 8.636993)
;

INSERT INTO cliente
VALUES
(1, 'Fernando Hermoso', '79686981P'),
(2, 'Alícia Ureña', '20397856F'),
(3, 'Carlos Fernando Oliveira', '06559115K'),
(4, 'Francesc Maroto', '39995886J'),
(5, 'Antoni Arnau', '86578600E'),
(6, 'Fernando Arjona', '28965625T'),
(7, 'Maria-Isabel Puerta', '13657411B'),
(8, 'Yesica Latorre', '20170117Z'),
(9, 'Joaquim Millan', '02728999A'),
(10, 'Isidro Calderon', '18994264J')
;

INSERT INTO producto
VALUES
(1, 'Test1', 13.10, 5.10),
(2, 'Test2', 5.03, 6),
(3, 'Test3', 1.13, 0.95),
(4, 'Test4', 3.37, 3.37),
(5, 'Test5', 0.57, 0.24),
(6, 'Test6', 2.99, 1.05),
(7, 'Test7', 0.5, 0.01),
(8, 'Test8', 3, 7.42),
(9, 'Test9', 2, 1.78),
(10, 'Test10', 1, 0.99)
;

INSERT INTO factura
VALUES
(1, 1, 13.10, 2.75, 15.85, 'pendiente'),
(2, 2, 5.03, 1.06, 6.09, 'pendiente'),
(3, 3, 1.13, 0.24, 1.37, 'pagada'),
(4, 4, 3.37, 0.71, 4.08, 'impagada'),
(5, 5, 0.57, 0.12, 0.69, 'impagada'),
(6, 6, 2.99, 0.63, 3.62, 'pagada'),
(7, 7, 0.5, 0.11, 0.61, 'pagada'),
(8, 8, 13.10, 2.75, 15.85, 'pagada'),
(9, 9, 5.03, 1.06, 6.09, 'pagada'),
(10, 10, 1.13, 0.24, 1.37, 'pagada'),
(11, 1, 9.37, 1.97, 11.34, 'pendiente'),
(12, 1, 0.57, 0.12, 0.69, 'pendiente'),
(13, 1, 2.99, 0.63, 3.62, 'impagada'),
(14, 1, 0.5, 0.11, 0.61, 'impagada'),
(15, 1, 13.10, 2.75, 15.85, 'pendiente'),
(16, 1, 5.03, 1.06, 6.09, 'pendiente'),
(17, 2, 1.13, 0.24, 1.37, 'pendiente'),
(18, 3, 3.37, 0.71, 4.08, 'pendiente'),
(19, 4, 0.57, 0.12, 0.69, 'pendiente'),
(20, 4, 2.99, 0.63, 3.62, 'pagada')
;

INSERT INTO factura_linea
VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 1),
(9, 9, 2),
(10, 10, 3),
(11, 11, 4),
(12, 12, 5),
(13, 13, 6),
(14, 14, 7),
(15, 15, 1),
(16, 16, 2),
(17, 17, 3),
(18, 18, 4),
(19, 19, 5),
(20, 20, 6),
(21, 11, 8),
(22, 11, 9),
(23, 11, 10)
;
