Se deberán realizar las consultas SQL para dar respuesta a las siguientes necesidades:

A. Obtención de los primeros '5' clientes ordenados por id ascendientemente.

RE: 

SELECT
	nombre,
	identificador_fiscal
FROM
	cliente
ORDER BY
	id ASC
LIMIT 5


B. Obtención de la suma de base imponible, iva y total de las facturas del cliente con
identificador fiscal '79686981P', así como el número de facturas para el mismo.

RE: 

SELECT
	ROUND(
		SUM(factura.base_imponible),
		2
	) AS Total_Base_Imponible,
	ROUND(SUM(factura.iva), 2) AS Total_IVA,
	ROUND(SUM(factura.total), 2) AS Total_Monto,
	count(factura.id) AS Total_Facturas
FROM
	factura
INNER JOIN cliente ON factura.cliente_id = cliente.id
WHERE
	cliente.identificador_fiscal = '79686981P'


C. Eliminar todas las facturas de los clientes que empiezan por 'Fernando' y con una
base imponible mayor o igual a '10€'.

RE:


DELETE tfactura, factura_linea
FROM
	factura tfactura
INNER JOIN cliente ON tfactura.cliente_id = cliente.id
INNER JOIN factura_linea on tfactura.id = factura_linea.factura_id
WHERE
	cliente.nombre LIKE 'Fernando%'
AND tfactura.base_imponible >= 10


D. Obtener todas las facturas con el producto 4.

RE:


SELECT
	factura.id AS NumeroFactura,
	producto.nombre AS NombreProducto
FROM
	factura
INNER JOIN factura_linea ON factura.id = factura_linea.factura_id
INNER JOIN producto ON factura_linea.producto_id = producto.id
WHERE
	factura_linea.producto_id = 4


E. Actualizar el estado de pago a 'pagada' de todas las facturas con el precio de venta
del producto mayor que el precio de coste.

RE:

UPDATE factura
INNER JOIN factura_linea ON factura_linea.factura_id = factura.id
INNER JOIN producto ON factura_linea.producto_id = producto.id
SET factura.estado_pago = 'pagada'
WHERE
	producto.precio_venta > producto.precio_coste