## Este sitio usa Laravel Framework 5.8 

##### Es importe antes de iniciar saber que es necesario:
    - php: 7.2.* (Minimo)
    - composer: 2.* (Recomendado)
    - MariaDB 10.3 (Minimo)

##### Despues abra una consola y ejecute:
      \>: composer update
      
##### Para iniciar el proyecto:
      http://127.0.0.1/nexttic/public
      
##### Recuerde importar el script SQL y cambiar los datos de conexion en el archivo .env
      /nexttic/.env      

## Acerca del test SQL
##### En este directorio
         nexttic/database/dbtest