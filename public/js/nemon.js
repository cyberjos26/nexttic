function InitTarifasComparativa() {
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /*Declaramos todas las variables de la factura*/

        var $consumoP1 = $("#consumoP1");
        var $consumoP2 = $("#consumoP2");
        var $consumoP3 = $("#consumoP3");
        var $importeEnergiaP1 = $("#importeEnergiaP1");
        var $importeEnergiaP2 = $("#importeEnergiaP2");
        var $importeEnergiaP3 = $("#importeEnergiaP3");
        var $potenciaP1 = $("#potenciaP1");
        var $potenciaP2 = $("#potenciaP2");
        var $importePotenciaP1 = $("#importePotenciaP1");
        var $importePotenciaP2 = $("#importePotenciaP2");
        var $numeroDiasFactura = $("#numeroDias");
        var $importeContadorFactura = $("#importeContadorFactura");
        var $importeTotalFacturaClienteFactura = $("#importeTotalFacturaClienteFactura");

        var $divDatosFactura = $("#divDatosFactura");
        var $divCompartiva = $("#divCompartiva");
        var $divTieneAhorro = $(".tieneAhorro");
        var $btncalcularAhorro = $("#calcularAhorro");
        var $btnRecalcular = $("#recalcular");
        var $tieneAhorro = "";

        /*Declaramos todas las variables de la comparativa*/

        var $importeEnergiaCliente = $("#importeEnergiaCliente");
        var $importePotenciaCliente = $("#importePotenciaCliente");
        var $importeEnergiaNemon = $("#importeEnergiaNemon");
        var $importePotenciaNemon = $("#importePotenciaNemon");
        var $importeTotalFacturaNemon = $("#importeTotalFacturaNemon");
        var $importeContadorComparativa = $("#importeContadorComparativa");
        var $importeTotalFacturaClienteComparativa = $("#importeTotalFacturaClienteComparativa");

        /*Generalidades*/
        var $cargando = $("#cargando");

        ///////////////////// Llamamos funciones ////////////////////////////


        function LimpiarCampos() {
            $consumoP1.val(0);
            $consumoP2.val(0);
            $consumoP3.val(0);
            $importeEnergiaP1.val(0);
            $importeEnergiaP2.val(0);
            $importeEnergiaP3.val(0);
            $potenciaP1.val(0);
            $potenciaP2.val(0);
            $importePotenciaP1.val(0);
            $importePotenciaP2.val(0);
            $numeroDiasFactura.val(0);
            $importeContadorFactura.val(0);
            $importeTotalFacturaClienteFactura.val(0);
            $importeTotalFacturaNemon.val(0);

            $("#textoValidacionConsumoP1").text("");
            $("#textoValidacionConsumoP2").text("");
            $("#textoValidacionConsumoP3").text("");
            $("#textoValidacionImporteEnergiaP1").text("");
            $("#textoValidacionImporteEnergiaP2").text("");
            $("#textoValidacionImporteEnergiaP3").text("");
            $("#textoValidacionPotenciaP1").text("");
            $("#textoValidacionPotenciaP2").text("");
            $("#textoValidacionImportePotenciaP1").text("");
            $("#textoValidacionImportePotenciaP2").text("");
            $("#textoValidacionNumeroDias").text("");
            $("#textoValidacionImporteContador").text("");
            $("#textoValidacionImporteTotalFactura").text("");
        }

        function LimpiarBorde() {
            $("#consumoP1").attr('style', 'none');
            $("#consumoP2").attr('style', 'none');
            $("#consumoP3").attr('style', 'none');
            $("#importeEnergiaP1").attr('style', 'none');
            $("#importeEnergiaP2").attr('style', 'none');
            $("#importeEnergiaP3").attr('style', 'none');
            $("#potenciaP1").attr('style', 'none');
            $("#potenciaP2").attr('style', 'none');
            $("#numeroDias").attr('style', 'none');
            $("#importeContadorFactura").attr('style', 'none');
            $("#importeTotalFacturaClienteFactura").attr('style', 'none');
        }

        function BloquearCampos() {
            $importeEnergiaCliente.attr("disabled", true);
            $importePotenciaCliente.attr("disabled", true);
            $importeEnergiaNemon.attr("disabled", true);
            $importePotenciaNemon.attr("disabled", true);
            $importeTotalFacturaNemon.attr("disabled", true);
            $importeContadorComparativa.attr("disabled", true);
            $importeTotalFacturaClienteComparativa.attr("disabled", true);
        }

        function ValidarCapaPresentacion($arreglo) {
            $arreglo.forEach(function ($arreglo, index) {

                $.each($arreglo[0], function ($index, $contenido) {

                    if ($index === 'consumoP1') {
                        $("#consumoP1").attr('style', 'border: 1px solid red');
                        $("#textoValidacionConsumoP1").text($contenido);
                    }
                    else if ($index === 'consumoP2') {
                        $("#consumoP2").attr('style', 'border: 1px solid red');
                        $("#textoValidacionConsumoP2").text($contenido);
                    }
                    else if ($index === 'consumoP3') {
                        $("#consumoP3").attr('style', 'border: 1px solid red');
                        $("#textoValidacionConsumoP3").text($contenido);
                    }
                    else if ($index === 'importeConsumoP1') {
                        $("#importeEnergiaP1").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImporteEnergiaP1").text($contenido);
                    }
                    else if ($index === 'importeConsumoP2') {
                        $("#importeEnergiaP2").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImporteEnergiaP2").text($contenido);
                    }
                    else if ($index === 'importeConsumoP3') {
                        $("#importeEnergiaP3").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImporteEnergiaP3").text($contenido);
                    }
                    else if ($index === 'potenciaP1') {
                        $("#potenciaP1").attr('style', 'border: 1px solid red');
                        $("#textoValidacionPotenciaP1").text($contenido);
                    }
                    else if ($index === 'potenciaP2') {
                        $("#potenciaP2").attr('style', 'border: 1px solid red');
                        $("#textoValidacionPotenciaP2").text($contenido);
                    }
                    else if ($index === 'importePotenciaP1') {
                        $("#importePotenciaP1").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImportePotenciaP1").text($contenido);
                    }
                    else if ($index === 'importePotenciaP2') {
                        $("#importePotenciaP2").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImportePotenciaP2").text($contenido);
                    }
                    else if ($index === 'numeroDiasFactura') {
                        $("#numeroDias").attr('style', 'border: 1px solid red');
                        $("#textoValidacionNumeroDias").text($contenido);
                    }
                    else if ($index === 'importeContadorFactura') {
                        $("#importeContadorFactura").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImporteContador").text($contenido);
                    }
                    else if ($index === 'importeTotalFacturaClienteFactura') {
                        $("#importeTotalFacturaClienteFactura").attr('style', 'border: 1px solid red');
                        $("#textoValidacionImporteTotalFactura").text($contenido);
                    }

                });
            });
        }


        $btncalcularAhorro.on("click", function () {
            $.ajax
            ({
                url: 'api/validardatoscliente',
                type: 'POST',
                data: {
                    'consumoP1': $consumoP1.val(),
                    'consumoP2': $consumoP2.val(),
                    'consumoP3': $consumoP3.val(),
                    'importeConsumoP1': $importeEnergiaP1.val(),
                    'importeConsumoP2': $importeEnergiaP2.val(),
                    'importeConsumoP3': $importeEnergiaP3.val(),
                    'potenciaP1': $potenciaP1.val(),
                    'potenciaP2': $potenciaP2.val(),
                    'importePotenciaP1': $importePotenciaP1.val(),
                    'importePotenciaP2': $importePotenciaP2.val(),
                    'numeroDiasFactura': $numeroDiasFactura.val(),
                    'importeContadorFactura': $importeContadorFactura.val(),
                    'importeTotalFacturaClienteFactura': $importeTotalFacturaClienteFactura.val()
                },
                dataType: 'json',
                beforeSend: function () {
                    $divDatosFactura.hide();
                    $cargando.show();
                },
                success: function (data) {
                    if (data[0].exitoso !== 1) {
                        /*Limpiamos la validacion capa de presentacion*/
                        LimpiarBorde();

                        var $arreglo = data;
                        $divDatosFactura.show();
                        $cargando.hide();

                        /*Aplicamos reglas de capa de presentación*/
                        ValidarCapaPresentacion($arreglo);

                    }
                    else {
                        $.ajax
                        ({
                            url: 'api/datoscliente',
                            type: 'POST',
                            data: {
                                'consumoP1': $consumoP1.val(),
                                'consumoP2': $consumoP2.val(),
                                'consumoP3': $consumoP3.val(),
                                'importeConsumoP1': $importeEnergiaP1.val(),
                                'importeConsumoP2': $importeEnergiaP2.val(),
                                'importeConsumoP3': $importeEnergiaP3.val(),
                                'potenciaP1': $potenciaP1.val(),
                                'potenciaP2': $potenciaP2.val(),
                                'importePotenciaP1': $importePotenciaP1.val(),
                                'importePotenciaP2': $importePotenciaP2.val(),
                                'numeroDiasFactura': $numeroDiasFactura.val(),
                                'importeContadorFactura': $importeContadorFactura.val(),
                                'importeTotalFacturaClienteFactura': $importeTotalFacturaClienteFactura.val()
                            },
                            dataType: 'json',
                            beforeSend: function () {
                                $divDatosFactura.hide();
                                $cargando.show();
                            },
                            success: function (data) {
                                $cargando.hide();

                                /*Mostramos comparativa*/
                                $divCompartiva.show();

                                /*Ocultamos facturacion*/
                                $divDatosFactura.hide();

                                /*Determinamos si hay un ahorro*/
                                $tieneAhorro = parseInt(data.tieneAhorro);
                                $divTieneAhorro.text('');

                                BloquearCampos();

                                $importeEnergiaCliente.val(data.importeEnergiaCliente);
                                $importePotenciaCliente.val(data.importePotenciaCliente);
                                $importeEnergiaNemon.val(data.importeEnergiaNemon);
                                $importePotenciaNemon.val(data.importePotenciaNemon);
                                $importeContadorComparativa.val(data.importeContador);
                                $importeTotalFacturaClienteComparativa.val(data.importeTotalFacturaCliente);
                                $importeTotalFacturaNemon.val(data.importeTotalFacturaNemon);


                                if ($tieneAhorro === 1) {
                                    $divTieneAhorro.text('Se ha calculado un ahorro de ' + parseFloat(data.ahorroFactura).toFixed(2) + ' € en su factura si nos contrata');
                                }

                            },
                            error: function (xhr) {
                                alert("No hemos podido procesar su solicitud, si el problema persiste comuniquese con support@nemon.com");
                                console.log(xhr.responseText);
                                location.reload();

                            }
                        });
                    }


                },
                error: function (xhr) {
                    alert("No hemos podido procesar su solicitud, si el problema persiste comuniquese con support@nemon.com");
                    console.log(xhr.responseText);
                    location.reload();

                }
            });


        });

        $btnRecalcular.on("click", function () {
            LimpiarCampos();
            location.reload();

        });

        /*Cambiamos comas por punto*/
        $("input:text").on('keyup', function () {
            $valorTexto = $(this).val();
            $resultado = $valorTexto.replace(/,/g, '.');
            $(this).val($resultado);
        });


    });
}