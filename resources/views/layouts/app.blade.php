<!DOCTYPE html>
<html lang="en">
<head>
{!! Html::script('/js/jquery-3.1.1.min.js') !!} <!--1-->
{!! Html::script('/js/nemon.js') !!} <!--1-->
{!! Html::script('/js/bootstrap.min.js') !!} <!--5-->
@yield('script') <!--2-->
{!! Html::style('/css/bootstrap.min.css') !!} <!--3-->
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('html')
    @yield('title')
</head>
<body>
<br>
@yield('content')
@yield('endscript')
</body>
</html>