@extends('layouts.app')
@section('title')
    <title>Compare la tarifa de la luz</title>
    @endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @include('mensajes.loading')
                <div id="divDatosFactura">
                    @include('tarifas.partials.fieldsfactura')
                </div>
                <div id="divCompartiva" style="display: none">
                    @include('tarifas.partials.fieldscomparativa')
                </div>
            </div>
    </div>
    </div>
@endsection
@section('endscript')
    <script>
        InitTarifasComparativa();
    </script>
@endsection
