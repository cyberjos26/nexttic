<div class="panel panel-default">
    <div class="panel-heading"><h2>Resultado Compartiva</h2></div>
    <div class="panel-body">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                <h3>Su factura</h3>
                <hr/>
                <div class="form-group">
                    {!! Form::label('importeenergiacliente','Importe energia') !!}
                    <div class="input-group">
                        {!! Form::text('importeenergiacliente',0,['id'=>'importeEnergiaCliente','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                    <br>
                    {!! Form::label('importepotenciacliente','Importe potencia') !!}
                    <div class="input-group">
                        {!! Form::text('importepotenciacliente',0,['id'=>'importePotenciaCliente','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                <h3>Con nosotros</h3>
                <hr/>
                <div class="form-group">
                    {!! Form::label('importeenergianemon','Importe energia') !!}
                    <div class="input-group">
                        {!! Form::text('importeenergianemon',0,['id'=>'importeEnergiaNemon','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                    <br>
                    {!! Form::label('importepotencianemon','Importe potencia') !!}
                    <div class="input-group">
                        {!! Form::text('importepotencianemon',0,['id'=>'importePotenciaNemon','class'=>'form-control', 'aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                <hr/>
                <div class="form-group">
                    {!! Form::label('importecontador','Alquiler contador') !!}
                    <div class="input-group">
                        {!! Form::text('importeContador',0,['id'=>'importeContadorComparativa','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                <hr/>
            </div>
            <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                <div class="form-group">
                    {!! Form::label('importetotalfacturacliente','Total') !!}
                    <div class="input-group">
                        {!! Form::text('importetotalfacturacliente',0,['id'=>'importeTotalFacturaClienteComparativa','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                <div class="form-group">
                    {!! Form::label('importetotalfacturanemon','Total') !!}
                    <div class="input-group">
                        {!! Form::text('importetotalfacturanemon',0,['id'=>'importeTotalFacturaNemon','class'=>'form-control','aria-describedby'=>'basic-addon2']) !!}
                        <span class="input-group-addon" id="basic-addon2">€</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tieneAhorro">
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="row">
            <div class="text-center">
                <br>
                {!! Form::button('Recalcular',['id'=>'recalcular','class'=>'btn btn-primary' ]) !!}
            </div>
        </div>
    </div>

</div>