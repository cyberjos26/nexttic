<div class="panel panel-default">
    <div class="panel-heading"><h2>Introduzca los datos de su factura actual</h2></div>
    <div class="panel-body">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <h3>Importe de energía por periodos</h3>
                <hr/>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('consumop1','Consumo P1') !!}
                        <div class="input-group">
                            {!! Form::text('consumop1',0,['id'=>'consumoP1','class'=>'form-control','placeholder'=>'Consumo en punta','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">kWh</span>
                        </div>
                        <span id="textoValidacionConsumoP1" class="textoValidacion"></span>
                        <br>
                        {!! Form::label('importeenergiap1','Importe energia P1') !!}
                        <div class="input-group">
                            {!! Form::text('importeenergiap1',0,['id'=>'importeEnergiaP1','class'=>'form-control','placeholder'=>'Ingrese el importe en punta','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImporteEnergiaP1" class="textoValidacion"></span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('consumop2','Consumo P2') !!}
                        <div class="input-group">
                            {!! Form::text('consumop2',0,['id'=>'consumoP2','class'=>'form-control','placeholder'=>'Consumo en llano','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">kWh</span>
                        </div>
                        <span id="textoValidacionConsumoP2" class="textoValidacion"></span>
                        <br>
                        {!! Form::label('importeenergiap2','Importe energia P2') !!}
                        <div class="input-group">
                            {!! Form::text('importeenergiap2',0,['id'=>'importeEnergiaP2','class'=>'form-control','placeholder'=>'Ingrese el importe en llano','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImporteEnergiaP2" class="textoValidacion"></span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('consumop3','Consumo P3') !!}
                        <div class="input-group">
                            {!! Form::text('consumop3',0,['id'=>'consumoP3','class'=>'form-control','placeholder'=>'Consumo en valle','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">kWh</span>
                        </div>
                        <span id="textoValidacionConsumoP3" class="textoValidacion"></span>
                        <br>
                        {!! Form::label('importeenergiap3','Importe energia P3') !!}
                        <div class="input-group">
                            {!! Form::text('importeenergiap3',0,['id'=>'importeEnergiaP3','class'=>'form-control','placeholder'=>'Ingrese el importe en valle','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImporteEnergiaP3" class="textoValidacion"></span>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <h3>Importe de potencia por periodos</h3>
                <hr/>
                <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                    <div class="form-group">
                        {!! Form::label('potenciap1','Potencia P1') !!}
                        <div class="input-group">
                            {!! Form::text('potenciap1',0,['id'=>'potenciaP1','class'=>'form-control','placeholder'=>'Potencia en punta','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">kW</span>
                        </div>
                        <span id="textoValidacionPotenciaP1" class="textoValidacion"></span>
                        <br>
                        {!! Form::label('importepotenciap1','Importe potencia P1') !!}
                        <div class="input-group">
                            {!! Form::text('importepotenciap1',0,['id'=>'importePotenciaP1','class'=>'form-control','placeholder'=>'Ingrese el importe en punta','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImportePotenciaP1" class="textoValidacion"></span>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                    <div class="form-group">
                        {!! Form::label('potenciap2','Potencia P2') !!}
                        <div class="input-group">
                            {!! Form::text('potenciap2',0,['id'=>'potenciaP2','class'=>'form-control','placeholder'=>'Potencia en llano','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">kW</span>
                        </div>
                        <span id="textoValidacionPotenciaP2" class="textoValidacion"></span>
                        <br>
                        {!! Form::label('importepotenciap2','Importe potencia P2') !!}
                        <div class="input-group">
                            {!! Form::text('importepotenciap2',0,['id'=>'importePotenciaP2','class'=>'form-control','placeholder'=>'Ingrese el importe en llano','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImportePotenciaP2" class="textoValidacion"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <h3>Dias, contador y monto de su factura</h3>
                <hr/>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('numerodias','Número de dias') !!}
                        <div class="input-group">
                            {!! Form::text('numerodias',0,['id'=>'numeroDias','class'=>'form-control','placeholder'=>'Ingrese los dias de su facturación','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">Días</span>
                        </div>
                        <span id="textoValidacionNumeroDias" class="textoValidacion"></span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('importecontador','Importe alquiler contador') !!}
                        <div class="input-group">
                            {!! Form::text('importecontador',0,['id'=>'importeContadorFactura','class'=>'form-control','placeholder'=>'Ingrese el coste de alquiler del contador','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImporteContador" class="textoValidacion"></span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12 col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('importetotalfactura','Importe total factura') !!}
                        <div class="input-group">
                            {!! Form::text('importetotalfactura',0,['id'=>'importeTotalFacturaClienteFactura', 'class'=>'form-control','placeholder'=>'Ingrese el importe total de su factura','aria-describedby'=>'basic-addon2']) !!}
                            <span class="input-group-addon" id="basic-addon2">€</span>
                        </div>
                        <span id="textoValidacionImporteTotalFactura" class="textoValidacion"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="row">
                    <div class="text-center">
                        <br>
                        {!! Form::button('Calcular ahorro',['id'=>'calcularAhorro','class'=>'btn btn-success' ]) !!}
                    </div>
                </div>
            </div>
        </div>
      </div>

