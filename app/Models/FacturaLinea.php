<?php

/**
 * Created by Reliese Model.
 */

namespace nexttic\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FacturaLinea
 * 
 * @property int $id
 * @property int|null $factura_id
 * @property int|null $producto_id
 * 
 * @property Factura|null $factura
 * @property Producto|null $producto
 *
 * @package nexttic\Models
 */
class FacturaLinea extends Model
{
	protected $table = 'factura_linea';
	public $timestamps = false;

	protected $casts = [
		'factura_id' => 'int',
		'producto_id' => 'int'
	];

	protected $fillable = [
		'factura_id',
		'producto_id'
	];

	public function factura()
	{
		return $this->belongsTo(Factura::class);
	}

	public function producto()
	{
		return $this->belongsTo(Producto::class);
	}
}
