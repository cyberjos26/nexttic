<?php

/**
 * Created by Reliese Model.
 */

namespace nexttic\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Producto
 * 
 * @property int $id
 * @property string|null $nombre
 * @property float|null $precio_venta
 * @property float|null $precio_coste
 * 
 * @property Collection|FacturaLinea[] $factura_lineas
 *
 * @package nexttic\Models
 */
class Producto extends Model
{
	protected $table = 'producto';
	public $timestamps = false;

	protected $casts = [
		'precio_venta' => 'float',
		'precio_coste' => 'float'
	];

	protected $fillable = [
		'nombre',
		'precio_venta',
		'precio_coste'
	];

	public function factura_lineas()
	{
		return $this->hasMany(FacturaLinea::class);
	}
}
