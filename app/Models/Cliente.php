<?php

/**
 * Created by Reliese Model.
 */

namespace nexttic\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cliente
 * 
 * @property int $id
 * @property string|null $nombre
 * @property string|null $identificador_fiscal
 * 
 * @property Collection|Factura[] $facturas
 *
 * @package nexttic\Models
 */
class Cliente extends Model
{
	protected $table = 'cliente';
	public $timestamps = false;

	protected $fillable = [
		'nombre',
		'identificador_fiscal'
	];

	public function facturas()
	{
		return $this->hasMany(Factura::class);
	}
}
