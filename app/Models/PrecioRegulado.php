<?php

/**
 * Created by Reliese Model.
 */

namespace nexttic\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use nexttic\Http\Clases\BI\Funcionalidad\Arrays\ArrayFun;
use nexttic\Http\Clases\BI\Persistencia\Tarifa\TarifaPersistencia;

/**
 * Class PrecioRegulado
 * 
 * @property int $id
 * @property string|null $tipo
 * @property string|null $tarifa
 * @property Carbon|null $fecha_inicio
 * @property Carbon|null $fecha_fin
 * @property float|null $p1
 * @property float|null $p2
 * @property float|null $p3
 * @property float|null $p4
 * @property float|null $p5
 * @property float|null $p6
 *
 * @package nexttic\Models
 */
class PrecioRegulado extends Model
{
	protected $table = 'precio_regulado';
	public $timestamps = false;

	protected $casts = [
		'p1' => 'float',
		'p2' => 'float',
		'p3' => 'float',
		'p4' => 'float',
		'p5' => 'float',
		'p6' => 'float'
	];

	protected $dates = [
		'fecha_inicio',
		'fecha_fin'
	];

	protected $fillable = [
		'tipo',
		'tarifa',
		'fecha_inicio',
		'fecha_fin',
		'p1',
		'p2',
		'p3',
		'p4',
		'p5',
		'p6'
	];

    public function CalculoImporteEnergiaPorPeriodos($consumoP1, $consumoP2, $consumoP3, $importeConsumoP1, $importeConsumoP2, $importeConsumoP3)
    {
        $tarifaEnergiaRegulada = new TarifaPersistencia();
        $tarifaEnergia = $tarifaEnergiaRegulada->ObtenerTarifaEnergiaReguladaNemon();

        $importeEnergiaNemonP1 = $tarifaEnergia->p1 * $consumoP1;
        $importeEnergiaNemonP2 = $tarifaEnergia->p2 * $consumoP2;
        $importeEnergiaNemonP3 = $tarifaEnergia->p3 * $consumoP3;

        /*En este caso no estamos calculando los precios para el cliente ya que el cliente los ha puesto en el formulario, necesitamos hacer la comparativa*/
        $totalImporteEnergiaCliente = $importeConsumoP1 + $importeConsumoP2 + $importeConsumoP3;

        /*Totalisamos el importe de enrgia de Nemon*/
        $totalImporteEnergiaNemon = $importeEnergiaNemonP1 + $importeEnergiaNemonP2 + $importeEnergiaNemonP3;

        $arrayEnergiaPorPeriodo =
            [
                'totalImporteEnergiaNemon'   => number_format($totalImporteEnergiaNemon,2),
                'totalImporteEnergiaCliente' => number_format($totalImporteEnergiaCliente,2)
            ];

        $importeEnergiaPorPeriodo = ArrayFun::ConvertirArrayObjetos($arrayEnergiaPorPeriodo);

        return $importeEnergiaPorPeriodo;
    }

    public function CalculoImportePotenciaPorPeriodos($potenciaP1, $potenciaP2, $importePotenciaP1, $importePotenciaP2, $numeroDiasFactura)
    {
        $tarifaEnergiaRegulada = new TarifaPersistencia();
        $tarifaEnergia = $tarifaEnergiaRegulada->ObtenerTarifaPotenciaReguladaNemon();

        $importePotenciaNemonP1 = ($tarifaEnergia->p1 * $potenciaP1 * $numeroDiasFactura)/365;
        $importePotenciaNemonP2 = ($tarifaEnergia->p2 * $potenciaP2 * $numeroDiasFactura)/365;

        /*En este caso no estamos calculando los precios para el cliente ya que el cliente los ha puesto en el formulario, necesitamos hacer la comparativa*/
        $totalImportePotenciaCliente = $importePotenciaP1 + $importePotenciaP2;

        /*Totalisamos el importe de potencia de Nemon*/
        $totalImportePotenciaNemon = $importePotenciaNemonP1 + $importePotenciaNemonP2;

        $arrayPotenciaPorPeriodo =
            [
                'totalImportePotenciaNemon' => number_format($totalImportePotenciaNemon,2),
                'totalImportePotenciaCliente' => number_format($totalImportePotenciaCliente,2)
            ];

        $importePotenciaPorPeriodo =  ArrayFun::ConvertirArrayObjetos($arrayPotenciaPorPeriodo);

        return $importePotenciaPorPeriodo;
    }

    public function CalculoBaseImpuestoElectrico($importeEnergiaPorPeriodo, $importePotenciaPorPeriodo)
    {

        $baseImpuestoElectricoNemon =  $importeEnergiaPorPeriodo->totalImporteEnergiaNemon + $importePotenciaPorPeriodo->totalImportePotenciaNemon;

        $arrayBaseImpuestoElectrico =
            [
                'baseImpuestoElectricoNemon' => number_format($baseImpuestoElectricoNemon,2)
            ];

        $baseImpuestoElectrico = ArrayFun::ConvertirArrayObjetos($arrayBaseImpuestoElectrico);

        return $baseImpuestoElectrico;

    }

    public function CalculoImpuestoElectrico($baseImpuestoElectrico)
    {
        $valorImpuestoElectrico = env('IMPUESTO_ELECTRICO');

        $calculoImpuestoElectricoNemon = ($baseImpuestoElectrico->baseImpuestoElectricoNemon *  $valorImpuestoElectrico) / 100;

        $arrayImpuestoElectrico =
            [
                'impustoElectricoNemon' => number_format($calculoImpuestoElectricoNemon,2)
            ];

        $impuestoElectrico = ArrayFun::ConvertirArrayObjetos($arrayImpuestoElectrico);

        return $impuestoElectrico;
    }
}
