<?php

/**
 * Created by Reliese Model.
 */

namespace nexttic\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use nexttic\Http\Clases\BI\Funcionalidad\Arrays\ArrayFun;

/**
 * Class Factura
 * 
 * @property int $id
 * @property int|null $cliente_id
 * @property float|null $base_imponible
 * @property float|null $iva
 * @property float|null $total
 * @property string|null $estado_pago
 * 
 * @property Cliente|null $cliente
 * @property Collection|FacturaLinea[] $factura_lineas
 *
 * @package nexttic\Models
 */
class Factura extends Model
{
	protected $table = 'factura';
	public $timestamps = false;

	protected $casts = [
		'cliente_id' => 'int',
		'base_imponible' => 'float',
		'iva' => 'float',
		'total' => 'float'
	];

	protected $fillable = [
		'cliente_id',
		'base_imponible',
		'iva',
		'total',
		'estado_pago'
	];

	public function cliente()
	{
		return $this->belongsTo(Cliente::class);
	}

	public function factura_lineas()
	{
		return $this->hasMany(FacturaLinea::class);
	}

    public function CalculoBaseIVA($baseImpuestoElectrico, $impuestoElectrico, $importeContador)
    {
        $baseIVANemon = $baseImpuestoElectrico->baseImpuestoElectricoNemon + $impuestoElectrico->impustoElectricoNemon + $importeContador;

        $arrayBaseIVA =
        [
          'baseIVANemon' => number_format($baseIVANemon,2),
        ];

        $baseIVA = ArrayFun::ConvertirArrayObjetos($arrayBaseIVA);

        return $baseIVA;
    }

    public function CalculoIVA($baseIVA)
    {
        $IVANemon = ($baseIVA->baseIVANemon * 21) / 100;

        $arrayIVA =
            [
                'IVANemon' => number_format($IVANemon,2),
            ];

        $IVA = ArrayFun::ConvertirArrayObjetos($arrayIVA);

        return $IVA;
    }

    public function ImporteTotalFactura($baseIVA, $IVA)
    {
        $importeTotalFacturaNemon = $baseIVA->baseIVANemon + $IVA->IVANemon;

        $arrayImporteTotalFactura =
            [
                'importeTotalFacturaNemon' => number_format($importeTotalFacturaNemon,2),
            ];

        $importeTotalFactura = ArrayFun::ConvertirArrayObjetos($arrayImporteTotalFactura);

        return $importeTotalFactura;

    }

    public function ImporteAhorroFactura($importeTotalFacturaCliente, $importeTotalFacturaNemon)
    {
        $tieneAhorro = 0;
        $importeAhorroFactura = 0;

        if($importeTotalFacturaCliente > $importeTotalFacturaNemon)
        {
            $importeAhorroFactura = $importeTotalFacturaCliente - $importeTotalFacturaNemon;
            $tieneAhorro = 1;
        }

        $arrayImporteAhorroFactura =
            [
                'importeAhorroFactura' => $importeAhorroFactura,
                'tieneAhorro' => $tieneAhorro
            ];

        $importeAhorroFactura = ArrayFun::ConvertirArrayObjetos($arrayImporteAhorroFactura);

        return $importeAhorroFactura;
    }
}
