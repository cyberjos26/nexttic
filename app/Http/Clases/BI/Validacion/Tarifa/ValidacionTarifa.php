<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 11/9/2021
 * Time: 5:09 PM
 */

namespace nexttic\Http\Clases\BI\Validacion\Tarifa;


use Illuminate\Support\Facades\Validator;
use nexttic\Http\Clases\BI\Funcionalidad\Arrays\ArrayFun;


class ValidacionTarifa
{

    public function ValidatDatosFormulario($datosFormulario)
    {
        $reglas =
            [
                'consumoP1' => 'required|numeric',
                'consumoP2' => 'required|numeric',
                'consumoP3' => 'required|numeric',
                'importeConsumoP1' => 'required|numeric',
                'importeConsumoP2' => 'required|numeric',
                'importeConsumoP3' => 'required|numeric',
                'potenciaP1' => 'required|numeric',
                'potenciaP2' => 'required|numeric',
                'importePotenciaP1' => 'required|numeric',
                'importePotenciaP2' => 'required|numeric',
                'numeroDiasFactura' =>  'required|numeric',
                'importeContadorFactura' => 'required|numeric',
                'importeTotalFacturaClienteFactura' => 'required|numeric',
            ];

        $mensajes =
            [
                'consumoP1.required' => 'El consumo de kWh en punta es requerido',
                'consumoP1.numeric' => 'El consumo de kWh en punta debe ser númerico',
                'consumoP2.required' => 'El consumo de kWh en llano es requerido',
                'consumoP2.numeric' => 'El consumo de kWh en llano debe ser númerico',
                'consumoP3.required' => 'El consumo de kWh en valle es requerido',
                'consumoP3.numeric' => 'El consumo de kWh en valle debe ser númerico',
                'importeConsumoP1.required' => 'El importe en consumo punta es requerido',
                'importeConsumoP1.numeric' => 'El importe debe ser númerico',
                'importeConsumoP2.required' => 'El importe en consumo llano es requerido',
                'importeConsumoP2.numeric' => 'El importe debe ser númerico',
                'importeConsumoP3.required' => 'El importe en consumo valle es requerido',
                'importeConsumoP3.numeric' => 'El importe debe ser númerico',
                'potenciaP1.required' => 'La potencia en kW en punta es requerido',
                'potenciaP1.numeric' => 'La potencia en kW en punta debe ser númerico',
                'potenciaP2.required' => 'La potencia en kW en llano es requerido',
                'potenciaP2.numeric' => 'La potencia en kW en llano debe ser númerico',
                'importePotenciaP1.required' =>  'El importe de potencia punta es requerido',
                'importePotenciaP2.required' =>  'El importe de potencia llano es requerido',
                'importePotenciaP1.numeric' =>  'El importe de potencia punta debe ser númerico',
                'importePotenciaP2.numeric' =>  'El importe de potencia llano debe ser númerico',
                'numeroDiasFactura.required' => 'El número de dias es requerido',
                'numeroDiasFactura.numeric' => 'El número de dias debe ser númerico',
                'importeContadorFactura.requerid' => 'El importe del contador es requerido',
                'importeContadorFactura.numeric' => 'El importe del contador debe ser númerico',
                'importeTotalFacturaClienteFactura.requerid' => 'El importe de la factura es requerido',
                'importeTotalFacturaClienteFactura.numeric' => 'El importe de la factura debe ser númerico',

            ];

        $v = Validator::make($datosFormulario, $reglas, $mensajes);

        if($v->fails())
        {
            $arrayErrores =
            [
                'exitoso' => 0,
                $v->errors()

            ];
        }
        else
        {
            $arrayErrores =
            [
                'exitoso' => 1

            ];
        }
        $errores = ArrayFun::ConvertirArrayObjetos($arrayErrores);

        return $errores;

    }
}