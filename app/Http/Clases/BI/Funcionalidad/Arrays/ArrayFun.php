<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 11/9/2021
 * Time: 9:56 AM
 */

namespace nexttic\Http\Clases\BI\Funcionalidad\Arrays;


class ArrayFun
{
    public static function ConvertirArrayObjetos($array)
    {
        $arrayPlano = json_encode($array);
        $objArray = json_decode($arrayPlano);

        return $objArray;
    }
}