<?php
/**
 * Created by PhpStorm.
 * User: Jose
 * Date: 9/9/2021
 * Time: 10:03 PM
 */

namespace nexttic\Http\Clases\BI\Persistencia\Tarifa;


use nexttic\Models\PrecioRegulado;

class TarifaPersistencia
{
    public function ObtenerTarifaEnergiaReguladaNemon()
    {
        $tarifaEnergiaRegulada = PrecioRegulado::select('precio_regulado.p1','precio_regulado.p2','precio_regulado.p3')
            ->where('tipo','=','energia')
            ->where('tarifa','=','2.0TD')
            ->where('fecha_inicio','=','2021-01-01')
            ->where('fecha_fin','=','2021-12-31')
            ->first();

        return $tarifaEnergiaRegulada;

    }

    public function ObtenerTarifaPotenciaReguladaNemon()
    {
        $tarifaPotenciaRegulada = PrecioRegulado::select('precio_regulado.p1','precio_regulado.p2')
            ->where('tipo','=','potencia')
            ->where('tarifa','=','2.0TD')
            ->where('fecha_inicio','=','2021-01-01')
            ->where('fecha_fin','=','2021-12-31')
            ->first();

        return $tarifaPotenciaRegulada;
    }
}