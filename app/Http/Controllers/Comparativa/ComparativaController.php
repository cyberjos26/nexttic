<?php

namespace nexttic\Http\Controllers\Comparativa;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use nexttic\Http\Clases\TarifaPer\TarifaPersistencia;
use nexttic\Models\Cliente;

class ComparativaController extends Controller
{
  public function index()
  {
      return view('tarifas.comparativa');
  }
}
