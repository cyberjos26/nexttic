<?php

namespace nexttic\Http\Controllers\Tarifas;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use nexttic\Models\Factura;
use nexttic\Models\PrecioRegulado;

class TarifaController extends Controller
{

    public function DatosCliente(Request $request)
    {
        if($request->ajax())
        {
            $factura = new Factura();
            $precioRegulado = new PrecioRegulado();
            /*Datos consumo*/
            $consumoP1 = $request->consumoP1;
            $consumoP2 = $request->consumoP2;
            $consumoP3 = $request->consumoP3;

            /*Datos importe de consumo*/
            $importeConsumoP1 = $request->importeConsumoP1;
            $importeConsumoP2 = $request->importeConsumoP2;
            $importeConsumoP3 = $request->importeConsumoP3;

            /*Datos potencia*/
            $potenciaP1 = $request->potenciaP1;
            $potenciaP2 = $request->potenciaP2;


            /*Datos importe de potencia*/
            $importePotenciaP1 = $request->importePotenciaP1;
            $importePotenciaP2 = $request->importePotenciaP2;

            /*Datos adicionales de la factura*/
            $numeroDiasFactura = $request->numeroDiasFactura;
            $importeContador = $request->importeContadorFactura;
            $importeTotalFacturaCliente = $request->importeTotalFacturaClienteFactura;


            /*Calculamos el importe de energia por periodo*/
            $importeEnergiaPorPeriodo = $precioRegulado->CalculoImporteEnergiaPorPeriodos($consumoP1, $consumoP2, $consumoP3, $importeConsumoP1, $importeConsumoP2, $importeConsumoP3);

            /*Calculamos el importe de potencia por periodo*/
            $importePotenciaPorPeriodo = $precioRegulado->CalculoImportePotenciaPorPeriodos($potenciaP1, $potenciaP2, $importePotenciaP1, $importePotenciaP2, $numeroDiasFactura);

            /*Calculamos base de impuesto electrico*/
            $baseImpuestoElectrico = $precioRegulado->CalculoBaseImpuestoElectrico($importeEnergiaPorPeriodo, $importePotenciaPorPeriodo);

            /*Calculamos impuesto electrico*/
            $impuestoElectrico = $precioRegulado->CalculoImpuestoElectrico($baseImpuestoElectrico);

            /*Calculamos base del IVA*/
            $baseIVA = $factura->CalculoBaseIVA($baseImpuestoElectrico,$impuestoElectrico,$importeContador);

            /*Calculamos IVA*/
            $IVA = $factura->CalculoIVA($baseIVA);

            /*Calculamos importe total factura*/
            $importeTotalFacturaNemon = $factura->ImporteTotalFactura($baseIVA,$IVA);

            /*Calculamos ahorro en factura*/
            $importeAhorroFactura = $factura->ImporteAhorroFactura($importeTotalFacturaCliente,$importeTotalFacturaNemon->importeTotalFacturaNemon);

            $ahorroFactura = $importeAhorroFactura->importeAhorroFactura;
            $tieneAhorro = $importeAhorroFactura->tieneAhorro;

            return response()->json
            ([
              'importeEnergiaNemon'     =>  $importeEnergiaPorPeriodo->totalImporteEnergiaNemon,
              'importeEnergiaCliente'   =>  $importeEnergiaPorPeriodo->totalImporteEnergiaCliente,
              'importePotenciaNemon'    =>  $importePotenciaPorPeriodo->totalImportePotenciaNemon,
              'importePotenciaCliente'  =>  $importePotenciaPorPeriodo->totalImportePotenciaCliente,
              'importeContador'         =>  $importeContador,
              'importeTotalFacturaNemon'  =>  $importeTotalFacturaNemon->importeTotalFacturaNemon,
              'importeTotalFacturaCliente' => $importeTotalFacturaCliente,
              'ahorroFactura' => $ahorroFactura,
              'tieneAhorro'   => $tieneAhorro

            ],200);

        }
    }

}
