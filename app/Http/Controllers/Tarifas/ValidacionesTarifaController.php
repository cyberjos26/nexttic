<?php

namespace nexttic\Http\Controllers\Tarifas;

use Illuminate\Http\Request;
use nexttic\Http\Clases\BI\Validacion\Tarifa\ValidacionTarifa;
use nexttic\Http\Controllers\Controller;

class ValidacionesTarifaController extends Controller
{
    public function ValidarDatosCliente(Request $request)
    {
        if($request->ajax())
        {
            $validacion = new ValidacionTarifa();
            /*Datos consumo*/
            $consumoP1 = $request->consumoP1;
            $consumoP2 = $request->consumoP2;
            $consumoP3 = $request->consumoP3;

            /*Datos importe de consumo*/
            $importeConsumoP1 = $request->importeConsumoP1;
            $importeConsumoP2 = $request->importeConsumoP2;
            $importeConsumoP3 = $request->importeConsumoP3;

            /*Datos potencia*/
            $potenciaP1 = $request->potenciaP1;
            $potenciaP2 = $request->potenciaP2;


            /*Datos importe de potencia*/
            $importePotenciaP1 = $request->importePotenciaP1;
            $importePotenciaP2 = $request->importePotenciaP2;

            /*Datos adicionales de la factura*/
            $numeroDiasFactura = $request->numeroDiasFactura;
            $importeContador = $request->importeContadorFactura;
            $importeTotalFacturaCliente = $request->importeTotalFacturaClienteFactura;

            $datos =
                [
                   'consumoP1' =>  $consumoP1,
                   'consumoP2' =>  $consumoP2,
                   'consumoP3' =>  $consumoP3,
                   'importeConsumoP1' => $importeConsumoP1,
                   'importeConsumoP2' => $importeConsumoP2,
                   'importeConsumoP3' => $importeConsumoP3,
                   'potenciaP1' => $potenciaP1,
                   'potenciaP2' => $potenciaP2,
                   'importePotenciaP1' =>  $importePotenciaP1,
                   'importePotenciaP2' =>  $importePotenciaP2,
                   'numeroDiasFactura' =>  $numeroDiasFactura,
                   'importeContadorFactura' => $importeContador,
                   'importeTotalFacturaClienteFactura' => $importeTotalFacturaCliente
                ];



            $resultado = $validacion->ValidatDatosFormulario($datos);

            return response()->json
            ([
               $resultado
            ]);

        }

    }
}
